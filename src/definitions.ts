declare module "@capacitor/core" {
  interface PluginRegistry {
    TestCapPlugin: TestCapPluginPlugin;
  }
}

export interface TestCapPluginPlugin {
  echo(options: { value: string }): Promise<{value: string}>;
}
