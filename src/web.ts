import { WebPlugin } from '@capacitor/core';
import { TestCapPluginPlugin } from './definitions';

export class TestCapPluginWeb extends WebPlugin implements TestCapPluginPlugin {
  constructor() {
    super({
      name: 'TestCapPlugin',
      platforms: ['web']
    });
  }

  async echo(options: { value: string }): Promise<{value: string}> {
    console.log('ECHO', options);
    return options;
  }
}

const TestCapPlugin = new TestCapPluginWeb();

export { TestCapPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(TestCapPlugin);
