package me.tomsen.test.capplugin;

import android.util.Log;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

@NativePlugin()
public class TestCapPlugin extends Plugin {

    @PluginMethod()
    public void echo(PluginCall call) {
        Log.d("TestCapPlugin", "echo");

        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", value + " Updated");
        call.success(ret);
    }
}
