
  Pod::Spec.new do |s|
    s.name = 'TestCapPlugin'
    s.version = '0.0.1'
    s.summary = 'Test Capacitor PLugin'
    s.license = 'MIT'
    s.homepage = 'git@gitlab.com:etomsen/test-cap-plugin.git'
    s.author = 'Evgeny Tomsen'
    s.source = { :git => 'git@gitlab.com:etomsen/test-cap-plugin.git', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
  end